;;; SPDX-License-Identifier: GPL-3.0-or-later
;;; Copyright © 2023 Mathieu Laparie <mlaparie@disr.it>

(define-module (bluetuith)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages linux)
  #:use-module (guix download)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (ice-9 match)
  #:use-module (nonguix build-system binary))

(define-public bluetuith
  (package
    (name "bluetuith")
    (version "0.1.9")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://github.com/darkhz/bluetuith/releases/download/v"
                           version
                           "/bluetuith_" version
                           (match (%current-system)
                             ("x86_64-linux" "_Linux_x86_64")
                             ("i686-linux" "_Linux_i686")
                             ("aarch64-linux" "_Linux_arm64")
                             ("armhf-linux" "_Linux_armv7"))
                           ".tar.gz"))
       (sha256
        (base32
         (match (%current-system)
           ("x86_64-linux" "04ssnbbr2nwl89ygkp777317v9720slwmpdmkfl9grzmq6qbndqw")
           ("i686-linux" "04ssnbbr2nwl89ygkp777317v9720slwmpdmkfl9grzmq6qbndqw")
           ("aarch64-linux" "0h1hynihinvrpf1l67d5yxhrfhiq3x4xi84f3fzfcbwndd7lfsl9")
           ("armhf-linux" "04ssnbbr2nwl89ygkp777317v9720slwmpdmkfl9grzmq6qbndqw"))))))
    (supported-systems '("x86_64-linux" "i686-linux" "aarch64-linux" "armhf-linux"))
    (build-system binary-build-system)
    (arguments
     (list
      #:install-plan #~'(("bluetuith" "bin/"))
      #:strip-binaries? #t))
    (inputs (list bluez))
    (synopsis "TUI-based Bluetooth connection manager")
    (description
     "@command{bluetuith} is a TUI-based Bluetooth connection manager, which
can interact with Bluetooth adapters and devices.  It aims to be a replacement
to most Bluetooth managers, like @command{blueman}.")
    (home-page "https://github.com/darkhz/bluetuith")
    (license license:expat)))
